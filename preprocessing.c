/*
 * preprocessing.c
 *
 *  Created on: Sept 27, 2016
 *      Author: sreenivas
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdarg.h>
#include <time.h>
/*--------------------------------------------------------------------------------------*/
/*                                                                                      */
/* Pre-Processing Transformations for NL-MEANS ERROR MEASURE FOR TEXTURE CLASSIFICATION */
/*                                                                                      */
/*			(Copyright by Sreenivas Narasimha Murali 8/2016)                            */
/*                                                                                      */
/*--------------------------------------------------------------------------------------*/


/*
 consists of:
 - Pre-processing for NL-means Error Measure
 */


/*--------------------------------------------------------------------------*/

void alloc_vector

(double **vector,   /* vector */
		long   n1)         /* size */

/* allocates memory for a vector of size n1 */


{
	*vector = (double *) malloc (n1 * sizeof(double));
	if (*vector == NULL)
	{
		printf("alloc_vector: not enough memory available\n");
		exit(1);
	}
	return;
}

/*--------------------------------------------------------------------------*/

void alloc_matrix

(double ***matrix,  /* matrix */
		long   n1,         /* size in direction 1 */
		long   n2)         /* size in direction 2 */

/* allocates memory for matrix of size n1 * n2 */


{
	long i;

	*matrix = (double **) malloc (n1 * sizeof(double *));
	if (*matrix == NULL)
	{
		printf("alloc_matrix: not enough memory available\n");
		exit(1);
	}
	for (i=0; i<n1; i++)
	{
		(*matrix)[i] = (double *) malloc (n2 * sizeof(double));
		if ((*matrix)[i] == NULL)
		{
			printf("alloc_matrix: not enough memory available\n");
			exit(1);
		}
	}
	return;
}

/*--------------------------------------------------------------------------*/

void disalloc_vector

(double *vector,    /* vector */
		long   n1)         /* size */

/* disallocates memory for a vector of size n1 */

{
	free(vector);
	return;
}

/*--------------------------------------------------------------------------*/

void disalloc_matrix

(double **matrix,   /* matrix */
		long   n1,         /* size in direction 1 */
		long   n2)         /* size in direction 2 */

/* disallocates memory for matrix of size n1 * n2 */

{
	long i;

	for (i=0; i<n1; i++)
		free(matrix[i]);

	free(matrix);

	return;
}

/*--------------------------------------------------------------------------*/

void read_string

(char *v)         /* string to be read */

/*
 reads a long value v
 */

{
	fgets (v, 80, stdin);
	if (v[strlen(v)-1] == '\n')
		v[strlen(v)-1] = 0;
	return;
}

/*--------------------------------------------------------------------------*/

void read_long

(long *v)         /* value to be read */

/*
 reads a long value v
 */

{
	char   row[80];    /* string for reading data */

	fgets (row, 80, stdin);
	if (row[strlen(row)-1] == '\n')
		row[strlen(row)-1] = 0;
	sscanf(row, "%ld", &*v);
	return;
}

/*--------------------------------------------------------------------------*/

void read_double

(double *v)         /* value to be read */

/*
 reads a double value v
 */

{
	char   row[80];    /* string for reading data */

	fgets (row, 80, stdin);
	if (row[strlen(row)-1] == '\n')
		row[strlen(row)-1] = 0;
	sscanf(row, "%lf", &*v);
	return;
}

/*--------------------------------------------------------------------------*/

void read_pgm_and_allocate_memory

(const char  *file_name,    /* name of pgm file */
		long        *nx,           /* image size in x direction, output */
		long        *ny,           /* image size in y direction, output */
		double      ***u)          /* image, output */

/*
  reads a greyscale image that has been encoded in pgm format P5;
  allocates memory for the image u;
  adds boundary layers of size 1 such that
  - the relevant image pixels in x direction use the indices 1,...,nx
  - the relevant image pixels in y direction use the indices 1,...,ny
 */

{
	FILE   *inimage;    /* input file */
	char   row[80];     /* for reading data */
	long   i, j;        /* loop variables */

	/* open file */
	inimage = fopen (file_name, "rb");
	if (NULL == inimage)
	{
		printf ("could not open file '%s' for reading, aborting.\n", file_name);
		exit (1);
	}

	/* read header */
	fgets (row, 80, inimage);          /* skip format definition */
	fgets (row, 80, inimage);
	while (row[0]=='#')                /* skip comments */
		fgets (row, 80, inimage);
	sscanf (row, "%ld %ld", nx, ny);   /* read image size */
	fgets (row, 80, inimage);          /* read maximum grey value */

	/* allocate memory */
	alloc_matrix (u, (*nx)+2, (*ny)+2);

	/* read image data row by row */
	for (j=1; j<=(*ny); j++)
		for (i=1; i<=(*nx); i++)
			(*u)[i][j] = (double) getc(inimage);

	/* close file */
	fclose(inimage);

	return;

} /* read_pgm_and_allocate_memory */

void comment_line

(char* comment,       /* comment string (output) */
		char* lineformat,    /* format string for comment line */
		...)                 /* optional arguments */

/*
  Add a line to the comment string comment. The string line can contain plain
  text and format characters that are compatible with sprintf.
  Example call: print_comment_line(comment,"Text %f %d",float_var,int_var);
  If no line break is supplied at the end of the input string, it is added
  automatically.
 */

{
	char     line[80];
	va_list  arguments;

	/* get list of optional function arguments */
	va_start(arguments,lineformat);

	/* convert format string and arguments to plain text line string */
	vsprintf(line,lineformat,arguments);

	/* add line to total commentary string */
	strncat(comment,line,80);

	/* add line break if input string does not end with one */
	if (line[strlen(line)-1] != '\n')
		sprintf(comment,"%s\n",comment);

	/* close argument list */
	va_end(arguments);

	return;

} /* comment_line */

/*--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------*/
void write_pgm

(double  **u,          /* image, unchanged */
		long    nx,           /* image size in x direction */
		long    ny,           /* image size in y direction */
		char    *file_name,   /* name of pgm file */
		char    *comments)    /* comment string (set 0 for no comments) */

/*
  writes a greyscale image into a pgm P5 file;
 */

{
	FILE           *outimage;  /* output file */
	long           i, j;       /* loop variables */
	double         aux;        /* auxiliary variable */
	unsigned char  byte;       /* for data conversion */

	/* open file */
	outimage = fopen (file_name, "wb");
	if (NULL == outimage)
	{
		printf("Could not open file '%s' for writing, aborting\n", file_name);
		exit(1);
	}

	/* write header */
	fprintf (outimage, "P5\n");                  /* format */
	if (comments != 0)
		fprintf (outimage, comments);             /* comments */
	fprintf (outimage, "%ld %ld\n", nx, ny);     /* image size */
	fprintf (outimage, "255\n");                 /* maximal value */

	/* write image data */
	for (j=1; j<=ny; j++)
		for (i=1; i<=nx; i++)
		{
			aux = u[i][j] + 0.499999;    /* for correct rounding */
			if (aux < 0.0)
				byte = (unsigned char)(0.0);
			else if (aux > 255.0)
				byte = (unsigned char)(255.0);
			else
				byte = (unsigned char)(aux);
			fwrite (&byte, sizeof(unsigned char), 1, outimage);
		}

	/* close file */
	fclose (outimage);

	return;

} /* write_pgm */

/*--------------------------------------------------------------------------*/

void dummies

(double   **u,       /* image matrix */
		long     nx,        /* size in x direction */
		long     ny,        /* size in y direction */
		long     p)         /* dummy-size */

/* creates dummy boundaries of size p by mirroring */

{
	long  i, j;       /* loop variables */
	long  imax,jmax;  /* maximal indices */

	imax = nx + 2 * p - 1;
	jmax = ny + 2 * p - 1;

	for (j=0; j<=jmax; j++)
	{
		for (i=0; i<p; i++)
		{
			if (j < p)
				u[i][j] = u[2 * p - 1 - i][2 * p - 1 - j];
			else if ((j >= p) && (j <= ny+p-1))
				u[i][j] = u[2 * p - 1 - i][j];
			else
				u[i][j] = u[2 * p - 1 - i][jmax + ny - j];
		}
		for (i=imax-p+1; i<=imax; i++)
		{
			if (j < p)
				u[i][j] = u[imax + nx - i][2 * p - 1 - j];
			else if ((j >= p) && (j <= ny+p-1))
				u[i][j] = u[imax + nx - i][j];
			else
				u[i][j] = u[imax + nx - i][jmax + ny - j];
		}
	}

	for (i=p; i<=nx+p-1; i++)
	{
		for (j=0; j<p; j++)
			u[i][j] = u[i][2 * p - 1 - j];
		for (j=ny+p; j<=jmax; j++)
			u[i][j] = u[i][jmax + ny - j];
	}

	return;

} /* dummies */

/*--------------------------------------------------------------------------*/
void preprocessing(long nx, long ny, double **p1, double **p2,double pregoal, int r)
{
	long i, j, a, b, x, y;									/* Loop Variables */
	/* Temporary storage variables */
	double **f;
	double **g;
	/* Time saver variables */
	long imax, jmax;
	imax = nx + r - 1;
	jmax = ny + r - 1;
	alloc_matrix (&f, nx+2*r+2, ny+2*r+2);
	alloc_matrix (&g, nx+2*r+2, ny+2*r+2);
	/* Mirror boundary pixels */
	dummies (p1, nx, ny, r);
	dummies (p2, nx, ny, r);
	for (i=0; i < nx+2*r; i++)
	{
		for (j=0; j < ny+2*r; j++)
		{
			f[i][j]=p1[i][j];
			g[i][j]=p2[i][j];
			/* After transfering to temporary variable. Fill them with zeros, so that rank can computed and stored in p1, p2 */
			p1[i][j]=0;
			p2[i][j]=0;
		}
	}
	/* Rank Transformation */
	if(pregoal==1)
	{
		printf("\n Rank Transformation...");
		/* Iterate over all pixels. Boundary coniditions taken care off */
		for(i=r; i<=imax; i++)
		{
			for(j=r; j<=jmax; j++)
			{
				/* Rank value variables of the 2 images */
				long rank1=0;
				long rank2=0;
				for(a=-r; a<=r; a++)
				{
					for(b=-r; b<=r; b++)
					{
						/* Ignore centre pixel */
						if (!(a==0 && b==0))
						{
							if (f[i+a][j+b] < f[i][j])
								rank1=rank1+1;
							if (g[i+a][j+b] < g[i][j])
								rank2=rank2+1;
						}
					}
				}
				/* Storing rank and transforming it into 0 to 255 range.          */
				/* For rank values 0 is minimum, 8 is maximum for window size 3x3 */
				p1[i][j]=255*(rank1-0)/(8-0);
				p2[i][j]=255*(rank2-0)/(8-0);
			}
		}
	}
	/* Census Transformation */
	if(pregoal==2)
	{
		printf("\n Census Transformation...");
		/* Census Rank value variables and bits of individual census ranks */
		unsigned int census1 = 0;
		unsigned int census2 = 0;
		unsigned int bit1    = 0;
		unsigned int bit2    = 0;
		/* Iterate over all pixels. Boundary coniditions taken care off */
		for(i=r; i<=imax; i++)
		{
			for(j=r; j<=jmax; j++)
			{
				census1 = 0;
				census2 = 0;
				bit1    = 0;
				bit2    = 0;
				for(a=-r; a<=r; a++)
				{
					for(b=-r; b<=r; b++)
					{
						/* Ignore centre pixel */
						if (!(a==0 && b==0))
						{
							/* bit shift to left by 1. increase rank and store them in a 8 bit sequence. */
							/* Example: If 11001100 is the census value. 204 (decimal) is stored		 */
							census1 <<= 1;
							census2 <<= 1;
							if (f[i+a][j+b] < f[i][j])
							{	
								bit1=1;
							}
							else
							{
								bit1=0;
							}
							if (g[i+a][j+b] < g[i][j])
							{
								bit2=1;
							}
							else
							{
								bit2=0;
							}
							census1 = census1 + bit1;
							census2 = census2 + bit2;
						}
					}
				}
				/* Census rank values already in the range of 0 to 255. No need for rescaling */
				p1[i][j]=census1;
				p2[i][j]=census2;
			}
		}
	}
	/* Complete Rank Transformation */
	if(pregoal==3)
	{
		printf("\n Complete Rank Transformation...");
		/* Iterate over all pixels. Boundary coniditions taken care off */
		for(i=r; i<=nx+r-1; i++)
		{
			for(j=r; j<=ny+r-1; j++)
			{
				for(a=-r; a<=r; a++)
				{
					for(b=-r; b<=r; b++)
					{
						/* Accumulate (ADD) rank obtained at each pixel from every 3x3 neighborhood where the pixel is involved */
						for(x=-r; x<=r; x++)
						{
							for(y=-r; y<=r; y++)
							{
								if (f[i+a][j+b] > f[i+x][j+y])
									p1[i+a][j+b]=p1[i+a][j+b]+1;
								if (g[i+a][j+b] > g[i+x][j+y])
									p2[i+a][j+b]=p2[i+a][j+b]+1;
							}
						}
					}
				}
				/* Storing complete rank and transforming it into 0 to 255 range.                                               */
				/* For complete rank values 0 is minimum, 81 is maximum for window size 3x3 after accumulation of complete rank */
				p1[i][j]=255*(p1[i][j]-0)/(81-0);
				p2[i][j]=255*(p2[i][j]-0)/(81-0);
			}
		}
	}
	disalloc_matrix (f, nx+2*r, ny+2*r);
	disalloc_matrix (g, nx+2*r, ny+2*r);
}

int main ()

{
	/* Time variables */
	clock_t begin, end;
	begin = clock();
	double  totaltime;
	char    in[80];               /* for reading data */
	char    ref[80];			  /* for reading data */
	char    out1[80];             /* for reading data */
	char    out2[80];             /* for reading data */
	double  **p1;				  /* pre-processing image 1 */
	double  **p2;				  /* pre-processing image 2 */
	double  **f;                  /* original image   */
	double  **g;                  /* reference image  */
	long    i, j, k, l;           /* loop variable */
	long    nx, ny;               /* image size in x, y direction */
	long    imax, jmax;           /* help indices */
	double  pregoal;			  /*choice for pre-processing transformations*/
	char    comments[1600];
	printf ("\n");
	printf ("Pre-Processing Transformations for NL-means as an Error Measure for Texture Classification:\n\n");
	printf ("**************************************************\n\n");
	printf ("    Copyright 2016 by Sreenivas Narasimha Murali.   \n");
	printf ("    Dept. of Mathematics and Computer Science     \n");
	printf ("    Saarland University, Saarbruecken, Germany    \n\n");
	printf ("    All rights reserved. Unauthorized usage,      \n");
	printf ("    copying, hiring, and selling prohibited.      \n\n");
	printf ("    Send bug reports to                           \n");
	printf ("    murali@mia.uni-saarland.de                  \n\n");
	printf ("**************************************************\n\n");


	/* ---- read input image (pgm format P5) ---- */

	printf ("\nFirst input image (pgm):                     ");
	read_string (in);
	read_pgm_and_allocate_memory (in, &nx, &ny, &f);
	printf("\nSecond input image should be of same resolution as first  \n");
	printf ("\nSecond input image to compare (pgm):         ");
	read_string (ref);
	read_pgm_and_allocate_memory (ref, &nx, &ny, &g);

	printf("Pre-processing transformations:\n");
	printf("1.Rank\n2.Census\n3.Complete Rank\nMake your choice:");
	read_double (&pregoal);
	
	printf ("\n");
	printf ("Pretransformation of Image 1 (pgm):            ");
	read_string (out1);
	printf ("\n");
	printf ("Pretransformation of Image 2 (pgm):            ");
	read_string (out2);
	printf ("\n");
	/*window size for pre-processing transformations are default chosen 3x3 */
	long windowsize = 3;
	long r = (windowsize-1)/2; 
	if(pregoal!=1 && pregoal !=2 && pregoal !=3)
	{
		pregoal=2;
	}
	/* ---- allocate storage for u and method_noise ---- */
	alloc_matrix (&p1,           nx+2*r+2, ny+2*r+2);
	alloc_matrix (&p2,           nx+2*r+2, ny+2*r+2);


	/* ---- initialisations ---- */

	imax = nx + r - 1;
	jmax = ny + r - 1;

	for (j=r, l=1; j<=jmax; j++, l++)
	{

		for (i=r, k=1; i<=imax; i++, k++)
		{
			p1[i][j] = f[k][l];
			p2[i][j] = g[k][l];
		}

	}
	preprocessing(nx, ny, p1, p2, pregoal, r);
	/* copy result to f and g */
	for (j=r, l=1; j<=jmax; j++, l++)
	{
		for (i=r, k=1; i<=imax; i++, k++)
		{
			f[k][l] = p1[i][j];
			g[k][l] = p2[i][j];
		}
	}
	printf("\n Algorithm has finished running..\n");
	comments[0]='\0';

	comment_line (comments, "# Pre-Processing transformations\n");
	comment_line (comments, "# patch size m:         3x3\n");
	/* write image */
	write_pgm (f, nx, ny, out1, comments);
	write_pgm (g, nx, ny, out2, comments);
	printf ("pretransformed image %s successfully written\n\n", out1);
	printf ("pretransformed image %s successfully written\n\n", out2);
	/* ---- free memory  ---- */

	disalloc_matrix (f,            nx+2,     ny+2);
	disalloc_matrix (g,            nx+2,     ny+2);
	disalloc_matrix (p1,           nx+2*r+2, ny+2*r+2);
	disalloc_matrix (p2,           nx+2*r+2, ny+2*r+2);
	end=clock();
	totaltime=(double)(end - begin) / CLOCKS_PER_SEC;
	printf("total time taken is %f minutes\n", totaltime/60);
	return(0);
}
