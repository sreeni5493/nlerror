\beamer@endinputifotherversion {3.36pt}
\beamer@sectionintoc {1}{Problem}{3}{0}{1}
\beamer@subsectionintoc {1}{1}{Motivation}{3}{0}{1}
\beamer@subsectionintoc {1}{2}{Introduction}{4}{0}{1}
\beamer@sectionintoc {2}{Background Work}{5}{0}{2}
\beamer@subsectionintoc {2}{1}{Error Measures}{5}{0}{2}
\beamer@subsectionintoc {2}{2}{Texture Classification}{9}{0}{2}
\beamer@subsectionintoc {2}{3}{Non-Local Means}{14}{0}{2}
\beamer@sectionintoc {3}{Contribution}{16}{0}{3}
\beamer@subsectionintoc {3}{1}{Base Idea}{16}{0}{3}
\beamer@subsectionintoc {3}{2}{Similarity Structures}{17}{0}{3}
\beamer@subsectionintoc {3}{3}{Handling Invariances}{24}{0}{3}
\beamer@sectionintoc {4}{Results}{29}{0}{4}
