\BOOKMARK [1][-]{section.1}{Introduction}{}% 1
\BOOKMARK [2][-]{subsection.1.1}{Goals}{section.1}% 2
\BOOKMARK [3][-]{subsubsection.1.1.1}{Interpreting Non-Local means}{subsection.1.1}% 3
\BOOKMARK [3][-]{subsubsection.1.1.2}{Non-Local means as an error measure for texture classification}{subsection.1.1}% 4
\BOOKMARK [3][-]{subsubsection.1.1.3}{Importance of invariances in error measurement}{subsection.1.1}% 5
\BOOKMARK [2][-]{subsection.1.2}{Organization of the paper}{section.1}% 6
\BOOKMARK [1][-]{section.2}{Related Work}{}% 7
\BOOKMARK [2][-]{subsection.2.1}{Error Measure}{section.2}% 8
\BOOKMARK [3][-]{subsubsection.2.1.1}{Mean Square Error \(MSE\)}{subsection.2.1}% 9
\BOOKMARK [3][-]{subsubsection.2.1.2}{Structural Similarity \(SSIM\) Index}{subsection.2.1}% 10
\BOOKMARK [3][-]{subsubsection.2.1.3}{Multi-Scale Structural Similarity \(MS-SSIM\) Index}{subsection.2.1}% 11
\BOOKMARK [2][-]{subsection.2.2}{Texture classification}{section.2}% 12
\BOOKMARK [3][-]{subsubsection.2.2.1}{Local Binary Patterns}{subsection.2.2}% 13
\BOOKMARK [2][-]{subsection.2.3}{Non-Local means \(NL-means\)}{section.2}% 14
\BOOKMARK [3][-]{subsubsection.2.3.1}{Working}{subsection.2.3}% 15
\BOOKMARK [3][-]{subsubsection.2.3.2}{General results of NL-means}{subsection.2.3}% 16
\BOOKMARK [1][-]{section.3}{Base idea: Similarity structures}{}% 17
\BOOKMARK [2][-]{subsection.3.1}{Algorithm}{section.3}% 18
\BOOKMARK [2][-]{subsection.3.2}{Error values using NL-means}{section.3}% 19
\BOOKMARK [1][-]{section.4}{Parameter choices}{}% 20
\BOOKMARK [2][-]{subsection.4.1}{Penalization factor}{section.4}% 21
\BOOKMARK [2][-]{subsection.4.2}{Patch size}{section.4}% 22
\BOOKMARK [2][-]{subsection.4.3}{Window size}{section.4}% 23
\BOOKMARK [1][-]{section.5}{Invariances in error measure computation}{}% 24
\BOOKMARK [2][-]{subsection.5.1}{Illumination invariance}{section.5}% 25
\BOOKMARK [2][-]{subsection.5.2}{Rotational invariance}{section.5}% 26
\BOOKMARK [1][-]{section.6}{Pre-processing transformations}{}% 27
\BOOKMARK [2][-]{subsection.6.1}{Rank Transform}{section.6}% 28
\BOOKMARK [2][-]{subsection.6.2}{Census Transform}{section.6}% 29
\BOOKMARK [2][-]{subsection.6.3}{Complete Rank Transform}{section.6}% 30
\BOOKMARK [1][-]{section.7}{Results}{}% 31
\BOOKMARK [2][-]{subsection.7.1}{Error measure comparison}{section.7}% 32
\BOOKMARK [2][-]{subsection.7.2}{Texture classification database}{section.7}% 33
\BOOKMARK [1][-]{section.8}{Conclusion and outlook}{}% 34
\BOOKMARK [2][-]{subsection.8.1}{General conclusion}{section.8}% 35
\BOOKMARK [2][-]{subsection.8.2}{Future work}{section.8}% 36
\BOOKMARK [1][-]{section*.61}{References}{}% 37
