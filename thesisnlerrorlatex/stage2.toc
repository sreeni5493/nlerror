\contentsline {section}{\numberline {1}Introduction}{1}{section.1}
\contentsline {section}{\numberline {2}Osmosis}{1}{section.2}
\contentsline {section}{\numberline {3}Separating Material from Shape and Illumination}{3}{section.3}
\contentsline {section}{\numberline {4}Color subspace as photometric invariants}{6}{section.4}
\contentsline {section}{\numberline {5}Systems Engineering Papers}{7}{section.5}
\contentsline {subsection}{\numberline {5.1}Model-based skeleton design}{7}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Other Related Work}{9}{subsection.5.2}
\contentsline {section}{\numberline {6}Summary}{9}{section.6}
\contentsline {section}{References}{10}{section*.12}
