\contentsline {section}{\numberline {1}Introduction}{1}{section.1}
\contentsline {section}{\numberline {2}Edge-Enhancing diffusion}{2}{section.2}
\contentsline {section}{\numberline {3}Coherence Enhancing Diffusion}{4}{section.3}
\contentsline {section}{\numberline {4}Non-Local means}{6}{section.4}
\contentsline {section}{\numberline {5}Crisp Boundaries}{8}{section.5}
\contentsline {section}{\numberline {6}Structured Random Forests for Edge Detection}{10}{section.6}
\contentsline {section}{\numberline {7}Other Work: Brief report}{10}{section.7}
\contentsline {section}{\numberline {8}Summary and Future work}{11}{section.8}
\contentsline {section}{References}{12}{section*.14}
