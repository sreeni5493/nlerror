\contentsline {section}{\numberline {1}Abstract}{5}{section.1}
\contentsline {section}{\numberline {2}Introduction}{6}{section.2}
\contentsline {subsection}{\numberline {2.1}Goals}{7}{subsection.2.1}
\contentsline {subsubsection}{\numberline {2.1.1}Interpreting NL means}{7}{subsubsection.2.1.1}
\contentsline {subsubsection}{\numberline {2.1.2}Extending NL means as an error measure for texture classification}{7}{subsubsection.2.1.2}
\contentsline {subsubsection}{\numberline {2.1.3}Importance of Invariances in error measurement}{9}{subsubsection.2.1.3}
\contentsline {subsection}{\numberline {2.2}Organization of the paper}{9}{subsection.2.2}
\contentsline {section}{\numberline {3}Related Work}{10}{section.3}
\contentsline {subsection}{\numberline {3.1}Error Measure}{10}{subsection.3.1}
\contentsline {subsubsection}{\numberline {3.1.1}How are they useful?}{10}{subsubsection.3.1.1}
\contentsline {subsection}{\numberline {3.2}Texture and it's classification}{11}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Non-Local Means}{12}{subsection.3.3}
\contentsline {subsubsection}{\numberline {3.3.1}Working of NL means}{12}{subsubsection.3.3.1}
\contentsline {subsubsection}{\numberline {3.3.2}General results of NL means}{13}{subsubsection.3.3.2}
\contentsline {section}{\numberline {4}Other work in related areas}{15}{section.4}
\contentsline {subsection}{\numberline {4.1}Common Error Measures}{15}{subsection.4.1}
\contentsline {subsubsection}{\numberline {4.1.1}Mean Square Error(MSE)}{15}{subsubsection.4.1.1}
\contentsline {subsubsection}{\numberline {4.1.2}Structural Similarity Measurement Index(SSIM)}{16}{subsubsection.4.1.2}
\contentsline {subsubsection}{\numberline {4.1.3}MS-SSIM}{18}{subsubsection.4.1.3}
\contentsline {subsection}{\numberline {4.2}Texture Classification}{20}{subsection.4.2}
\contentsline {subsubsection}{\numberline {4.2.1}Local Binary Patterns}{20}{subsubsection.4.2.1}
\contentsline {section}{\numberline {5}Base Idea: Similarity Measure estimation}{22}{section.5}
\contentsline {subsection}{\numberline {5.1}Algorithm}{24}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Error values using NL means}{26}{subsection.5.2}
\contentsline {section}{\numberline {6}Parameter choices}{28}{section.6}
\contentsline {subsection}{\numberline {6.1}Penalization factor}{28}{subsection.6.1}
\contentsline {subsection}{\numberline {6.2}Patch size, Window Size}{29}{subsection.6.2}
\contentsline {section}{\numberline {7}Invariances in error measure computation}{31}{section.7}
\contentsline {subsection}{\numberline {7.1}Illumination invariance}{31}{subsection.7.1}
\contentsline {subsection}{\numberline {7.2}Rotational invariance}{32}{subsection.7.2}
\contentsline {section}{\numberline {8}Pre-processing transformations}{33}{section.8}
\contentsline {subsection}{\numberline {8.1}Rank Transform}{33}{subsection.8.1}
\contentsline {subsection}{\numberline {8.2}Census Transform}{34}{subsection.8.2}
\contentsline {subsection}{\numberline {8.3}Complete Rank Transform}{35}{subsection.8.3}
\contentsline {section}{\numberline {9}Results}{37}{section.9}
\contentsline {subsection}{\numberline {9.1}Error measure comparison}{37}{subsection.9.1}
\contentsline {subsection}{\numberline {9.2}Texture Classification database}{46}{subsection.9.2}
\contentsline {section}{\numberline {10}Conclusion and Outlook}{55}{section.10}
\contentsline {subsection}{\numberline {10.1}General Conclusion}{55}{subsection.10.1}
\contentsline {subsection}{\numberline {10.2}Drawbacks}{56}{subsection.10.2}
\contentsline {subsection}{\numberline {10.3}Future Work}{56}{subsection.10.3}
