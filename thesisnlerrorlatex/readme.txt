nlerror.c computes error between two input images using Non-Local means as presented in this thesis (thesis.pdf)
preprocessing.c does pre-processing transformation on an input image (Rank, Census or Complete Rank)

nlerror.c 
* Use single channel grey scale input images (P5 pgm format) of same size for computing error value between two images
* Initially there is a single choice to be made. Automatic or Manual
* Automatic:  .Patch size, window size, penalization factor automatically chosen (Patch size: 9x9, window size: 21x21, penalization factor: 130). 
              .Metric for comparing patches: Euclidean Distance of the  grey-value patches
              .Illumination invariance paramter (Alpha = 0) and rotational invariance parameter (Beta = 0). Invariance not accounted.
              .Four NL-means measures are done. To compare 2 images A and B, we compute self-similarity of A, cross-similarity of B with respect to A
               Compute error between the above two. Next, compute self-similarity of B, cross-similarity of A with respect to B. Compute error between
               these two. Average the two errors
* Manual: 	  .Patch size, window size, penalization factor chosen by user. You can choose between metrics to compare patches. 
			  .Either use euclidean distance of grey-value of patches or euclidean distance of the gradient of every pixel for every patch 
			  .Invariance paramaters are also manually chosen
			  .Have option to choose either single NL-means measure , or two NL-means measures or four NL-means measures.
			  .Single NL-means measure: To compare two images A and B. Compute cross-similarity of B with respect to A. Compute error
			   between this cross-similarity image and A. Fast but produces bad results
			  .Two NL-means measure: Compute self-similarity of A. Compute cross-similarity of B with respect to A. Compute error
			   between these two images. Slower than single NL-means. Better results. Disadvantage: Non-symmetric error measure 
			   (Error(A, B) != Error (B, A))
			  .Four NL-means measure : Same as automatic choice. Slow. Symmetric error measure
* Error values are displayed in output and also stored in a txt file (errorvalues.txt)

preprocessing.c
* Use single channel grey scale input image (P5 pgm format)
* Three Choices
* 1. Rank transformation
* 2. Census transformation
* 3. Complete Rank transformation

For bug reports or more information contact: sreenivas.nm5493@gmail.com (or) murali@mia.uni-saarland.de