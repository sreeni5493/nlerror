\contentsline {section}{\numberline {1}Introduction}{1}{section.1}
\contentsline {subsection}{\numberline {1.1}Goals}{2}{subsection.1.1}
\contentsline {subsubsection}{\numberline {1.1.1}Interpreting Non-Local means}{2}{subsubsection.1.1.1}
\contentsline {subsubsection}{\numberline {1.1.2}Non-Local means as an error measure for texture classification}{2}{subsubsection.1.1.2}
\contentsline {subsubsection}{\numberline {1.1.3}Importance of invariances in error measurement}{4}{subsubsection.1.1.3}
\contentsline {subsection}{\numberline {1.2}Organization of the paper}{4}{subsection.1.2}
\contentsline {section}{\numberline {2}Related Work}{5}{section.2}
\contentsline {subsection}{\numberline {2.1}Error Measure}{5}{subsection.2.1}
\contentsline {subsubsection}{\numberline {2.1.1}Mean Square Error (MSE)}{5}{subsubsection.2.1.1}
\contentsline {subsubsection}{\numberline {2.1.2}Structural Similarity (SSIM) Index}{8}{subsubsection.2.1.2}
\contentsline {subsubsection}{\numberline {2.1.3}Multi-Scale Structural Similarity (MS-SSIM) Index}{10}{subsubsection.2.1.3}
\contentsline {subsection}{\numberline {2.2}Texture classification}{12}{subsection.2.2}
\contentsline {subsubsection}{\numberline {2.2.1}Local Binary Patterns}{13}{subsubsection.2.2.1}
\contentsline {subsection}{\numberline {2.3}Non-Local means (NL-means)}{16}{subsection.2.3}
\contentsline {subsubsection}{\numberline {2.3.1}Working}{16}{subsubsection.2.3.1}
\contentsline {subsubsection}{\numberline {2.3.2}General results of NL-means}{17}{subsubsection.2.3.2}
\contentsline {section}{\numberline {3}Base idea: Similarity structures}{19}{section.3}
\contentsline {subsection}{\numberline {3.1}Algorithm}{21}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Error values using NL-means}{22}{subsection.3.2}
\contentsline {section}{\numberline {4}Parameter choices}{25}{section.4}
\contentsline {subsection}{\numberline {4.1}Penalization factor}{25}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Patch size}{26}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}Window size}{27}{subsection.4.3}
\contentsline {section}{\numberline {5}Invariances in error measure computation}{29}{section.5}
\contentsline {subsection}{\numberline {5.1}Illumination invariance}{29}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Rotational invariance}{30}{subsection.5.2}
\contentsline {section}{\numberline {6}Pre-processing transformations}{32}{section.6}
\contentsline {subsection}{\numberline {6.1}Rank Transform}{32}{subsection.6.1}
\contentsline {subsection}{\numberline {6.2}Census Transform}{33}{subsection.6.2}
\contentsline {subsection}{\numberline {6.3}Complete Rank Transform}{34}{subsection.6.3}
\contentsline {section}{\numberline {7}Results}{36}{section.7}
\contentsline {subsection}{\numberline {7.1}Error measure comparison}{36}{subsection.7.1}
\contentsline {subsection}{\numberline {7.2}Texture classification database}{54}{subsection.7.2}
\contentsline {section}{\numberline {8}Conclusion and outlook}{66}{section.8}
\contentsline {subsection}{\numberline {8.1}General conclusion}{66}{subsection.8.1}
\contentsline {subsection}{\numberline {8.2}Future work}{67}{subsection.8.2}
\contentsline {section}{References}{68}{section*.61}
