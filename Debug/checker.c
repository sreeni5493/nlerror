#include <stdio.h>  // fopen(), fclose(), fprintf(), FILE
#include <stdlib.h>

#include <errno.h>  // errno
#include <string.h> // strerror()

#define MAX_ROWS    (200)
#define MAX_COLS    (200)
#define BOARD_SIZE  (200)
#define SQUARE_SIZE (10)

int main (int argc, char* argv[])
{
    int row; // image row index
    int col; // image column index


    if( 2 != argc )
    { // then required command line parameter missing
        fprintf( stderr, "USAGE: %s <PGM_filename>\n", argv[0]);
        exit( EXIT_FAILURE );
    }

    // implied else, correct number of command line parameters

    /* Open the PGM file */
    FILE* image = fopen(argv[1], "wb");
    if (!image)
    {
      fprintf(stderr, "Can't open output file %s for write, due to: %s\n", argv[1], strerror( errno ) );
      exit( EXIT_FAILURE);
    }

    /*Write the header*/
    fprintf(image, "P5\n%d %d\n255\n", BOARD_SIZE, BOARD_SIZE);

    /* write the gray scale image */
    for (row = 0;row < BOARD_SIZE; row++)
    {
        for (col = 0;col < BOARD_SIZE; col++)
        {

            if( (row/SQUARE_SIZE)%2 == 0 )
            {
                if( (col/SQUARE_SIZE)%2 == 0 )
                {
                    fprintf( image, "%c", 150 );
                }

                else
                {
                    fprintf( image, "%c", 250 );//
                }
            }

            else
            {
                if( (col/SQUARE_SIZE)%2 == 0 )
                {
                    fprintf( image, "%c", 250 );
                }

                else
                {
                    fprintf( image, "%c", 150);
                }
            }
        }
    }

    fclose(image);

    return 0;
} // end function: main
