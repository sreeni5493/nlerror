
**************************************************

first input image (pgm):                      texture8sq.pgm
second input image to compare (pgm):          texture9sq.pgm

patch size m (integer):                 4
search window size n (integer):         10
std. dev. sigma (float):                130
alpha value:                            0.4
beta value:                             0.2
output image1 (pgm):                      tex98sq.pgm

output image2 (pgm):                      tex89sq.pgm

Applying NL-means filter with patch size 4, to compare textures 
window size 10 and std. dev. 130.0000


 computing error values now 
*****************************************************
error value(refout) for alpha 0.400000 and beta 0.200000 is 17.260790
*****************************************************
error value(refout) with ONLY alpha 0.400000 is 234.282349
*****************************************************
error value(refout) with ONLY beta 0.200000 is 893.804932
*****************************************************
error value(refout) without alpha and beta  is 1116.369141
*****************************************************
error value(outref) for alpha 0.400000 and beta 0.200000 is 40.766190
*****************************************************
error value(outref) with ONLY alpha 0.400000 is 387.446686
*****************************************************
error value(outref) with ONLY beta 0.200000 is 1443.302246
*****************************************************
error value(outref) without alpha and beta is 1805.090820
*****************************************************
overall error for alpha 0.400000 and beta 0.200000 is 29.013489
*****************************************************
overall error with ONLY alpha 0.400000 is 310.864502
*****************************************************
overall error with ONLY beta 0.200000 is 1168.553589
*****************************************************
overall error without alpha and beta is 1460.729980
*****************************************************
self similarity error of 1st texture is 1132.356812
*****************************************************
self similarity error of 2nd texture is 1958.137695
*****************************************************
output image-1 tex98sq.pgm successfully written

output image-2 tex89sq.pgm successfully written



**************************************************

first input image (pgm):                      texture8sq.pgm
second input image to compare (pgm):          texture7sq.pgm

patch size m (integer):                 4
search window size n (integer):         10
std. dev. sigma (float):                130
alpha value:                            0.4
beta value:                             0.2
output image1 (pgm):                      tex78sq.pgm

output image2 (pgm):                      tex87sq.pgm

Applying NL-means filter with patch size 4, to compare textures 
window size 10 and std. dev. 130.0000


 computing error values now 
*****************************************************
error value(refout) for alpha 0.400000 and beta 0.200000 is 1.439854
*****************************************************
error value(refout) with ONLY alpha 0.400000 is 24.131342
*****************************************************
error value(refout) with ONLY beta 0.200000 is 92.687546
*****************************************************
error value(refout) without alpha and beta  is 115.701721
*****************************************************
error value(outref) for alpha 0.400000 and beta 0.200000 is 26.298033
*****************************************************
error value(outref) with ONLY alpha 0.400000 is 77.989639
*****************************************************
error value(outref) with ONLY beta 0.200000 is 251.514587
*****************************************************
error value(outref) without alpha and beta is 315.396912
*****************************************************
overall error for alpha 0.400000 and beta 0.200000 is 13.868943
*****************************************************
overall error with ONLY alpha 0.400000 is 51.060490
*****************************************************
overall error with ONLY beta 0.200000 is 172.101074
*****************************************************
overall error without alpha and beta is 215.549316
*****************************************************
self similarity error of 1st texture is 175.233810
*****************************************************
self similarity error of 2nd texture is 745.491638
*****************************************************
output image-1 tex78sq.pgm successfully written

output image-2 tex87sq.pgm successfully written


**************************************************

first input image (pgm):                      texture8sq.pgm
second input image should be of same resolution as first  
second input image to compare (pgm):          texture7sq.pgm

patch size m (integer):                 4
search window size n (integer):         10
std. dev. sigma (float):                1300
alpha value:                            ^C

**************************************************

first input image (pgm):                      texture8sq.pgm
second input image should be of same resolution as first  
second input image to compare (pgm):          texture7sq.pgm

patch size m (integer):                 4
search window size n (integer):         10
std. dev. sigma (float):                130
alpha value:                            0.35
beta value:                             0.3
output image1(similarity image of second input w.r.t first) (pgm):tex78.pgm

output image2(similarity image of first input w.r.t second) (pgm):tex87.pgm

Applying NL-means filter with patch size 4, to compare textures 
window size 10 and std. dev. 130.0000


 computing error values now 
*****************************************************
error value(refout) for alpha 0.350000 and beta 0.300000 is 1.540819
*****************************************************
error value(refout) with ONLY alpha 0.350000 is 35.578049
*****************************************************
error value(refout) with ONLY beta 0.300000 is 81.184097
*****************************************************
error value(refout) without alpha and beta  is 115.701721
*****************************************************
error value(outref) for alpha 0.350000 and beta 0.300000 is 30.132021
*****************************************************
error value(outref) with ONLY alpha 0.350000 is 107.667923
*****************************************************
error value(outref) with ONLY beta 0.300000 is 219.557983
*****************************************************
error value(outref) without alpha and beta is 315.396912
*****************************************************
overall error for alpha 0.350000 and beta 0.300000 is 15.836420
*****************************************************
overall error with ONLY alpha 0.350000 is 71.622986
*****************************************************
overall error with ONLY beta 0.300000 is 150.371033
*****************************************************
overall error without alpha and beta is 215.549316
*****************************************************
self similarity error of 1st texture is 175.233810
*****************************************************
self similarity error of 2nd texture is 745.491638
*****************************************************
output image-1(similarity image of second input w.r.t first) tex78.pgm successfully written

output image-2(similarity image of first input w.r.t second) tex87.pgm successfully written
           

**************************************************

first input image (pgm):                      texture8sq.pgm
second input image should be of same resolution as first  
second input image to compare (pgm):          texture9sq.pgm

patch size m (integer):                 4
search window size n (integer):         10
std. dev. sigma (float):                130
alpha value:                            0.35
beta value:                             0.3
output image1(similarity image of second input w.r.t first) (pgm):tex98sq.pgm

output image2(similarity image of first input w.r.t second) (pgm):tex89sq.pgm

Applying NL-means filter with patch size 4, to compare textures 
window size 10 and std. dev. 130.0000


 computing error values now 
*****************************************************
error value(refout) for alpha 0.350000 and beta 0.300000 is 19.010288
*****************************************************
error value(refout) with ONLY alpha 0.350000 is 344.543121
*****************************************************
error value(refout) with ONLY beta 0.300000 is 782.535889
*****************************************************
error value(refout) without alpha and beta  is 1116.369141
*****************************************************
error value(outref) for alpha 0.350000 and beta 0.300000 is 44.640976
*****************************************************
error value(outref) with ONLY alpha 0.350000 is 564.651917
*****************************************************
error value(outref) with ONLY beta 0.300000 is 1262.414185
*****************************************************
error value(outref) without alpha and beta is 1805.090820
*****************************************************
overall error for alpha 0.350000 and beta 0.300000 is 31.825632
*****************************************************
overall error with ONLY alpha 0.350000 is 454.597534
*****************************************************
overall error with ONLY beta 0.300000 is 1022.475037
*****************************************************
overall error without alpha and beta is 1460.729980
*****************************************************
self similarity error of 1st texture is 1132.356812
*****************************************************
self similarity error of 2nd texture is 1958.137695
*****************************************************
output image-1(similarity image of second input w.r.t first) tex98sq.pgm successfully written

output image-2(similarity image of first input w.r.t second) tex89sq.pgm successfully written

