/*
 * nlerror.c
 *
 *  Created on: Feb 27, 2016
 *      Author: sreenivas
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdarg.h>
#include <time.h>
/*--------------------------------------------------------------------------*/
/*                                                                          */
/*          NL-MEANS ERROR MEASURE FOR TEXTURE CLASSIFICATION               */
/*                                                                          */
/* (Copyright by Sreenivas Narasimha Murali, Sven Grewenig, Pascal Peter    */
/*  and Joachim Weickert, 8/2016)                                           */
/*                                                                          */
/*--------------------------------------------------------------------------*/


/*
 consists of:
 - NL-Means Error Measure
 */


/*--------------------------------------------------------------------------*/

void alloc_vector

(double **vector,   /* vector */
		long   n1)         /* size */

/* allocates memory for a vector of size n1 */


{
	*vector = (double *) malloc (n1 * sizeof(double));
	if (*vector == NULL)
	{
		printf("alloc_vector: not enough memory available\n");
		exit(1);
	}
	return;
}

/*--------------------------------------------------------------------------*/

void alloc_matrix

(double ***matrix,  /* matrix */
		long   n1,         /* size in direction 1 */
		long   n2)         /* size in direction 2 */

/* allocates memory for matrix of size n1 * n2 */


{
	long i;

	*matrix = (double **) malloc (n1 * sizeof(double *));
	if (*matrix == NULL)
	{
		printf("alloc_matrix: not enough memory available\n");
		exit(1);
	}
	for (i=0; i<n1; i++)
	{
		(*matrix)[i] = (double *) malloc (n2 * sizeof(double));
		if ((*matrix)[i] == NULL)
		{
			printf("alloc_matrix: not enough memory available\n");
			exit(1);
		}
	}
	return;
}

/*--------------------------------------------------------------------------*/

void disalloc_vector

(double *vector,    /* vector */
		long   n1)         /* size */

/* disallocates memory for a vector of size n1 */

{
	free(vector);
	return;
}

/*--------------------------------------------------------------------------*/

void disalloc_matrix

(double **matrix,   /* matrix */
		long   n1,         /* size in direction 1 */
		long   n2)         /* size in direction 2 */

/* disallocates memory for matrix of size n1 * n2 */

{
	long i;

	for (i=0; i<n1; i++)
		free(matrix[i]);

	free(matrix);

	return;
}

/*--------------------------------------------------------------------------*/

void read_string

(char *v)         /* string to be read */

/*
 reads a long value v
 */

{
	fgets (v, 80, stdin);
	if (v[strlen(v)-1] == '\n')
		v[strlen(v)-1] = 0;
	return;
}

/*--------------------------------------------------------------------------*/

void read_long

(long *v)         /* value to be read */

/*
 reads a long value v
 */

{
	char   row[80];    /* string for reading data */

	fgets (row, 80, stdin);
	if (row[strlen(row)-1] == '\n')
		row[strlen(row)-1] = 0;
	sscanf(row, "%ld", &*v);
	return;
}

/*--------------------------------------------------------------------------*/

void read_double

(double *v)         /* value to be read */

/*
 reads a double value v
 */

{
	char   row[80];    /* string for reading data */

	fgets (row, 80, stdin);
	if (row[strlen(row)-1] == '\n')
		row[strlen(row)-1] = 0;
	sscanf(row, "%lf", &*v);
	return;
}

/*--------------------------------------------------------------------------*/

void read_pgm_and_allocate_memory

(const char  *file_name,    /* name of pgm file */
		long        *nx,           /* image size in x direction, output */
		long        *ny,           /* image size in y direction, output */
		double      ***u)          /* image, output */

/*
  reads a greyscale image that has been encoded in pgm format P5;
  allocates memory for the image u;
  adds boundary layers of size 1 such that
  - the relevant image pixels in x direction use the indices 1,...,nx
  - the relevant image pixels in y direction use the indices 1,...,ny
 */

{
	FILE   *inimage;    /* input file */
	char   row[80];     /* for reading data */
	long   i, j;        /* loop variables */

	/* open file */
	inimage = fopen (file_name, "rb");
	if (NULL == inimage)
	{
		printf ("could not open file '%s' for reading, aborting.\n", file_name);
		exit (1);
	}

	/* read header */
	fgets (row, 80, inimage);          /* skip format definition */
	fgets (row, 80, inimage);
	while (row[0]=='#')                /* skip comments */
		fgets (row, 80, inimage);
	sscanf (row, "%ld %ld", nx, ny);   /* read image size */
	fgets (row, 80, inimage);          /* read maximum grey value */

	/* allocate memory */
	alloc_matrix (u, (*nx)+2, (*ny)+2);

	/* read image data row by row */
	for (j=1; j<=(*ny); j++)
		for (i=1; i<=(*nx); i++)
			(*u)[i][j] = (double) getc(inimage);

	/* close file */
	fclose(inimage);

	return;

} /* read_pgm_and_allocate_memory */

/*--------------------------------------------------------------------------*/

void comment_line

(char* comment,       /* comment string (output) */
		char* lineformat,    /* format string for comment line */
		...)                 /* optional arguments */

/*
  Add a line to the comment string comment. The string line can contain plain
  text and format characters that are compatible with sprintf.
  Example call: print_comment_line(comment,"Text %f %d",float_var,int_var);
  If no line break is supplied at the end of the input string, it is added
  automatically.
 */

{
	char     line[80];
	va_list  arguments;

	/* get list of optional function arguments */
	va_start(arguments,lineformat);

	/* convert format string and arguments to plain text line string */
	vsprintf(line,lineformat,arguments);

	/* add line to total commentary string */
	strncat(comment,line,80);

	/* add line break if input string does not end with one */
	if (line[strlen(line)-1] != '\n')
		sprintf(comment,"%s\n",comment);

	/* close argument list */
	va_end(arguments);

	return;

} /* comment_line */

/*--------------------------------------------------------------------------*/

void write_pgm

(double  **u,          /* image, unchanged */
		long    nx,           /* image size in x direction */
		long    ny,           /* image size in y direction */
		char    *file_name,   /* name of pgm file */
		char    *comments)    /* comment string (set 0 for no comments) */

/*
  writes a greyscale image into a pgm P5 file;
 */

{
	FILE           *outimage;  /* output file */
	long           i, j;       /* loop variables */
	double         aux;        /* auxiliary variable */
	unsigned char  byte;       /* for data conversion */

	/* open file */
	outimage = fopen (file_name, "wb");
	if (NULL == outimage)
	{
		printf("Could not open file '%s' for writing, aborting\n", file_name);
		exit(1);
	}

	/* write header */
	fprintf (outimage, "P5\n");                  /* format */
	if (comments != 0)
		fprintf (outimage, comments);             /* comments */
	fprintf (outimage, "%ld %ld\n", nx, ny);     /* image size */
	fprintf (outimage, "255\n");                 /* maximal value */

	/* write image data */
	for (j=1; j<=ny; j++)
		for (i=1; i<=nx; i++)
		{
			aux = u[i][j] + 0.499999;    /* for correct rounding */
			if (aux < 0.0)
				byte = (unsigned char)(0.0);
			else if (aux > 255.0)
				byte = (unsigned char)(255.0);
			else
				byte = (unsigned char)(aux);
			fwrite (&byte, sizeof(unsigned char), 1, outimage);
		}

	/* close file */
	fclose (outimage);

	return;

} /* write_pgm */

void dummies

(double   **u,       /* image matrix */
		long     nx,        /* size in x direction */
		long     ny,        /* size in y direction */
		long     p)         /* dummy-size */

/* creates dummy boundaries of size p by mirroring */

{
	long  i, j;       /* loop variables */
	long  imax,jmax;  /* maximal indices */

	imax = nx + 2 * p - 1;
	jmax = ny + 2 * p - 1;

	for (j=0; j<=jmax; j++)
	{
		for (i=0; i<p; i++)
		{
			if (j < p)
				u[i][j] = u[2 * p - 1 - i][2 * p - 1 - j];
			else if ((j >= p) && (j <= ny+p-1))
				u[i][j] = u[2 * p - 1 - i][j];
			else
				u[i][j] = u[2 * p - 1 - i][jmax + ny - j];
		}
		for (i=imax-p+1; i<=imax; i++)
		{
			if (j < p)
				u[i][j] = u[imax + nx - i][2 * p - 1 - j];
			else if ((j >= p) && (j <= ny+p-1))
				u[i][j] = u[imax + nx - i][j];
			else
				u[i][j] = u[imax + nx - i][jmax + ny - j];
		}
	}

	for (i=p; i<=nx+p-1; i++)
	{
		for (j=0; j<p; j++)
			u[i][j] = u[i][2 * p - 1 - j];
		for (j=ny+p; j<=jmax; j++)
			u[i][j] = u[i][jmax + ny - j];
	}

	return;

} /* dummies */

/*--------------------------------------------------------------------------*/

double nl_means

(		long     nx,      /* image dimension in x direction */
		long     ny,      /* image dimension in y direction */
		long     m,       /* patch size */
		double   delta,   /* penalization factor */
		long     t,       /* search window size */
		double   **u1,	  /* input: texture 1; output: Cross-Similarity of texture 1 with regards to texture 2 (Reference) */
		double   **u2,	  /* input: texture 2; output: Cross-Similarity of texture 2 with regards to texture 1 (Reference) */
		double   **x,	  /* Self-Similarity of texture 1 */
		double   **y,	  /* Self-Similarity of texture 2 */
		double   alpha,	  /* illumination invariance parameter */
		double   beta,	  /* rotational invariance parameter */
		double   goal1,	  /* goal 1 is for choosing between complexity of the algorithm */
		double   goal2)	  /* goal 2 is for choosing invariance */

/*
  performs NL-Means filtering
 */

{
	printf("Alpha value is %f, Beta value is %f",alpha,beta);
	printf("\nFor choice 1 you chose the following option: 1.");
	printf("\nFor choice 2 you chose the following option: %d.",(int)goal1);
	printf("\nFor choice 3 you chose the following option: %d.",(int)goal2);
	printf("\n");
	long    i, j, k, l, p, r;          								/* loop variables */
	long    imax, jmax;                								/* extended image boundaries */
	double  weight1,weight2,weightsum1, weightsum2, temp1,temp2;    /* time saving variables */
	double  **f;                       							    /* work copy of image u1 */
	double  **g;                       								/* work copy of image u2 */
	long    rmin, rmax, smin, smax;    								/* boundaries of search window */
	double weight3,weight4,weightsum3, weightsum4,tempself1,tempself2;
	/* create reflecting dummy boundaries */
	dummies (u1, nx, ny, m);
	dummies (u2, nx, ny, m);

	imax = nx + m - 1;
	jmax = ny + m - 1;

	alloc_matrix (&f, nx+2*m+2, ny+2*m+2);
	alloc_matrix (&g, nx+2*m+2, ny+2*m+2);
	/* copy u1 into f and u2 into g */
	for (i=0; i<nx+2*m; i++)
	{
		for (j=0; j<ny+2*m; j++)
		{
			f[i][j]=u1[i][j];
			g[i][j]=u2[i][j];
		}
	}


	/* ---- NL-Means Filter ---- */

	/* iterate over all pixels  */
	for (i=m; i<=imax; i++)
	{
		for (j=m; j<=jmax; j++)
		{
			u1[i][j]   = 0.0;
			u2[i][j]   = 0.0;
			x[i][j]    = 0.0;
			y[i][j]    = 0.0;
			weightsum1 = 0.0;
			weightsum2 = 0.0;
			weightsum3 = 0.0;
			weightsum4 = 0.0;
			/* compute boundaries of the search window */
			if (i-t < m)     rmin = m;
			else             rmin = i-t;
			if (i+t > imax)  rmax = imax;
			else             rmax = i+t;
			if (j-t < m)     smin = m;
			else             smin = j-t;
			if (j+t > jmax)  smax = jmax;
			else             smax = j+t;

			/* for each pixel in the search window... */
			for (k=rmin; k<=rmax; k++)
			{
				for (l=smin; l<=smax; l++)
				{
					temp1=0.0;
					temp2=0.0;
					tempself1=0.0;
					tempself2=0.0;
					/* compute patch distance (squared Euclidean norm) */
					/* goal1 = 1 computes single NL-means measure */
					if(goal1==1)
					{
						for (p=-m; p<=m; p++)
						{
							for (r=-m; r<=m; r++)
							{
								temp2    += (f[i-p][j-r]-g[k-p][l-r])*(f[i-p][j-r]-g[k-p][l-r]);
							}
						}
					}
					/* goal1 = 2 computes two NL-means measure */
					if(goal1==2)
					{
						for (p=-m; p<=m; p++)
						{
							for (r=-m; r<=m; r++)
							{
								temp2    += (f[i-p][j-r]-g[k-p][l-r])*(f[i-p][j-r]-g[k-p][l-r]);
								tempself1+= (f[i-p][j-r]-f[k-p][l-r])*(f[i-p][j-r]-f[k-p][l-r]);
							}
						}
					}
					/* goal1 = 3 computes four NL-means measure */
					if(goal1==3)
					{
						for (p=-m; p<=m; p++)
						{
							for (r=-m; r<=m; r++)
							{
								temp1    += (g[i-p][j-r]-f[k-p][l-r])*(g[i-p][j-r]-f[k-p][l-r]);
								temp2    += (f[i-p][j-r]-g[k-p][l-r])*(f[i-p][j-r]-g[k-p][l-r]);
								tempself1+= (f[i-p][j-r]-f[k-p][l-r])*(f[i-p][j-r]-f[k-p][l-r]);
								tempself2+= (g[i-p][j-r]-g[k-p][l-r])*(g[i-p][j-r]-g[k-p][l-r]);
							}
						}
					}


					/* compute the corresponding Gaussian weight. note that temp1 and temp2
                   already contain the squared Euclidean norm */
					if(goal1==1)
					{
						weight2     = exp (-temp2 / (2 * delta * delta));
						u2[i][j]   += weight2 * g[k][l];
						weightsum2 += weight2;
					}
					if(goal1==2)
					{
						weight2     = exp (-temp2 / (2 * delta * delta));
						weight3     = exp (-tempself1 / (2 * delta * delta));
						u2[i][j]   += weight2 * g[k][l];
						x[i][j]    += weight3 * f[k][l];
						weightsum2 += weight2;
						weightsum3 += weight3;
					}
					if(goal1==3)
					{
						weight1     = exp (-temp1 / (2 * delta * delta));
						weight2     = exp (-temp2 / (2 * delta * delta));
						weight3     = exp (-tempself1 / (2 * delta * delta));
						weight4     = exp (-tempself2 / (2 * delta * delta));
						u1[i][j]   += weight1 * f[k][l];
						u2[i][j]   += weight2 * g[k][l];
						x[i][j]    += weight3 * f[k][l];
						y[i][j]    += weight4 * g[k][l];
						weightsum1 += weight1;
						weightsum2 += weight2;
						weightsum3 += weight3;
						weightsum4 += weight4;
					}
				}

			}

			/* normalize result */
			if (weightsum1 > 0)
			{

				u1[i][j] = u1[i][j]/weightsum1;
			}

			else
			{
				u1[i][j] = f[i][j];
			}
			if (weightsum2 > 0)
			{
				u2[i][j] = u2[i][j]/weightsum2;
			}
			else
			{

				u2[i][j] = g[i][j];
			}

			if (weightsum3 > 0)
			{

				x[i][j] = x[i][j]/weightsum3;
			}
			else
			{

				x[i][j] = u2[i][j];
			}

			if (weightsum4 > 0)
			{

				y[i][j] = y[i][j]/weightsum4;
			}
			else
			{

				y[i][j] = u1[i][j];
			}
		}
	}
	/* Error Value compuatation */
	/* dummies in case of gradient for boundary pixels */
	dummies (u1, nx, ny, m+1);
	dummies (u2, nx, ny, m+1);
	dummies (f, nx, ny,  m+1);
	dummies (g, nx, ny,  m+1);
	dummies (x, nx, ny,  m+1);
	dummies (y, nx, ny,  m+1);
	/* Derivative values */
	double fxder, fyder;
	double u2xder, u1xder, xxder, yxder;
	double u2yder, u1yder, xyder, yyder;
	double errorvalue = 0.0;				/* Overal Error Value */
	/* Illumination derivative values */
	double illumfxder = 0.0;
	double illumfyder = 0.0;
	double illum1xder = 0.0;
	double illum1yder = 0.0;
	double illum2xder = 0.0;
	double illum2yder = 0.0;
	/* Rotational derivative values */
	double ru2der     = 0.0;
	double rxder      = 0.0;
	double rfder      = 0.0;
	double ru1der     = 0.0;
	double ryder      = 0.0;
	for (i=m; i<=imax; i++)
	{
		for (j=m; j<=jmax; j++)
		{

			if(goal1==1)
			{
				u2xder  = (u2[i+1][j]-u2[i][j]);
				fxder   = (f[i+1][j]-f[i][j]);
				u2yder  = (u2[i][j+1]-u2[i][j]);
				fyder   = (f[i][j+1]-f[i][j]);
			}
			if(goal1==2)
			{
				u2xder  = (u2[i+1][j]-u2[i][j]);
				xxder   = (x[i+1][j]-x[i][j]);
				u2yder  = (u2[i][j+1]-u2[i][j]);
				xyder   = (x[i][j+1]-x[i][j]);
			}
			if(goal1==3)
			{
				u2xder  = (u2[i+1][j]-u2[i][j]);
				u1xder  = (u1[i+1][j]-u1[i][j]);
				xxder   = (x[i+1][j]-x[i][j]);
				yxder   = (y[i+1][j]-y[i][j]);
				u2yder  = (u2[i][j+1]-u2[i][j]);
				u1yder  = (u1[i][j+1]-u1[i][j]);
				xyder   = (x[i][j+1]-x[i][j]);
				yyder   = (y[i][j+1]-y[i][j]);
			}
			illumfxder  = (u2xder-fxder)*(u2xder-fxder);
			illumfyder  = (u2yder-fyder)*(u2yder-fyder);
			illum1xder  = (u2xder-xxder)*(u2xder-xxder);
			illum1yder  = (u2yder-xyder)*(u2yder-xyder);
			illum2xder  = (u1xder-yxder)*(u1xder-yxder);
			illum2yder  = (u1yder-yyder)*(u1yder-yyder);
			ru2der      = (sqrt((u2xder*u2xder)+(u2yder*u2yder)));
			rxder       = (sqrt((xxder*xxder)+(xyder*xyder)));
			rfder		= (sqrt((fxder*fxder)+(fyder*fyder)));
			ru1der      = (sqrt((u1xder*u1xder)+(u1yder*u1yder)));
			ryder       = (sqrt((yxder*yxder)+(yyder*yyder)));
			if(goal1==1)
			{
				/* goal2 = 1, No invariance */
				if(goal2==1)
				{
					errorvalue += ((u2[i][j]-f[i][j])*(u2[i][j]-f[i][j]));
				}
				/* goal2 = 2, Illumination invariance */
				if(goal2==2)
				{
					errorvalue += ((1-(2*alpha))*((u2[i][j]-f[i][j])*(u2[i][j]-f[i][j]))
							      +(alpha*(illumfxder+illumfyder)));
				}
				/* goal2 = 3, Rotational invariance */
				if(goal2==3)
				{
					errorvalue += ((1-beta)*((u2[i][j]-f[i][j])*(u2[i][j]-f[i][j]))
								  +(beta*0.5*(rfder-ru2der)*(rfder-ru2der)));
				}
				/* goal2 = 4, Illumination and Rotational invariance */
				if(goal2==4)
				{
					errorvalue += ((1-(2*alpha)-beta)*((u2[i][j]-f[i][j])*(u2[i][j]-f[i][j]))
							      +(alpha*(illumfxder+illumfyder))+(beta*0.5*(rfder-ru2der)*(rfder-ru2der)));
				}
			}
			if(goal1==2)
			{
				/* goal2 = 1, No invariance */
				if(goal2==1)
				{
					errorvalue += (u2[i][j]-x[i][j])*(u2[i][j]-x[i][j]);
				}
				/* goal2 = 2, Illumination invariance */
				if(goal2==2)
				{
					errorvalue += ((1-(2*alpha))*((u2[i][j]-x[i][j])*(u2[i][j]-x[i][j]))
							      +(alpha*(illum1xder+illum1yder)));
				}
				/* goal2 = 3, Rotational invariance */
				if(goal2==3)
				{
					errorvalue += ((1-beta)*((u2[i][j]-x[i][j])*(u2[i][j]-x[i][j]))
							      +(beta*0.5*(rxder-ru2der)*(rxder-ru2der)));
				}
				/* goal2 = 4, Illumination and Rotational invariance */
				if(goal2==4)
				{
					errorvalue += ((1-(2*alpha)-beta)*((u2[i][j]-x[i][j])*(u2[i][j]-x[i][j]))
							      +(alpha*(illum1xder+illum1yder))+(beta*0.5*(rxder-ru2der)*(rxder-ru2der)));
				}
			}
			if(goal1==3)
			{
				/* goal2 = 1, No invariance */
				if(goal2==1)
				{
					errorvalue += 0.5*((u2[i][j]-x[i][j])*(u2[i][j]-x[i][j])
								  +((u1[i][j]-y[i][j])*(u1[i][j]-y[i][j])));
				}
				/* goal2 = 2, Illumination invariance */
				if(goal2==2)
				{
					errorvalue += 0.5*(((1-(2*alpha))*((u2[i][j]-x[i][j])*(u2[i][j]-x[i][j]))
									  +(alpha*(illum1xder+illum1yder)))
								  +
								  ((1-(2*alpha))*((u1[i][j]-y[i][j])*(u1[i][j]-y[i][j]))
									  +(alpha*(illum2xder+illum2yder))));
				}
				/* goal2 = 3, Rotational invariance */
				if(goal2==3)
				{
					errorvalue += 0.5*(((1-beta)*((u2[i][j]-x[i][j])*(u2[i][j]-x[i][j]))
							          +(beta*0.5*(rxder-ru2der)*(rxder-ru2der)))
							      +
							     ((1-beta)*((u1[i][j]-y[i][j])*(u1[i][j]-y[i][j]))
									+(beta*0.5*(ryder-ru1der)*(ryder-ru1der))));
				}
				/* goal2 = 4, Illumination and Rotational invariance */
				if(goal2==4)
				{
					errorvalue += 0.5*(((1-(2*alpha)-beta)*((u2[i][j]-x[i][j])*(u2[i][j]-x[i][j]))
							          +(alpha*(illum1xder+illum1yder))+(beta*0.5*(rxder-ru2der)*(rxder-ru2der)))
							      +
							      ((1-(2*alpha)-beta)*((u1[i][j]-y[i][j])*(u1[i][j]-y[i][j]))
									  +(alpha*(illum2xder+illum2yder))
									  +(beta*0.5*(ryder-ru1der)*(ryder-ru1der))));
				}
			}
		}
	}
	printf("*****************************************************");
	printf ("\n");
	errorvalue=errorvalue/((imax-m+1)*(jmax-m+1));
	printf("Error value is %f.\n",errorvalue);
	printf("*****************************************************");
	printf ("\n");
	disalloc_matrix (f, nx+2*m, ny+2*m);
	disalloc_matrix (g, nx+2*m, ny+2*m);
	return errorvalue;
} /* NL-means for texture classification */

double nl_means1

(		long     nx,      /* image dimension in x direction */
		long     ny,      /* image dimension in y direction */
		long     m,       /* patch size */
		double   delta,   /* penalization factor */
		long     t,       /* search window size */
		double   **u1,	  /* input: texture 1; output: similarity of texture 1 with regards to texture 2 (Reference) */
		double   **u2,	  /* input: texture 2; output: similarity of texture 2 with regards to texture 1 (Reference) */
		double   **x,	  /* Self-Similarity of texture 1 */
		double   **y,	  /* Self-Similarity of texture 2 */
		double   alpha,   /* illumination invariance parameter */
		double   beta,	  /* rotational invariance parameter */
		double   goal1,	  /* goal 1 is for choosing between complexity of the algorithm */
		double   goal2)   /* goal 2 is for choosing invariance */

/*
  performs NL-Means filtering
 */

{
	printf("Alpha value is %f, Beta value is %f",alpha,beta);
	printf("\nFor choice 1 you chose the following option: 2.");
	printf("\nFor choice 2 you chose the following option: %d.",(int)goal1);
	printf("\nFor choice 3 you chose the following option: %d.",(int)goal2);
	printf("\n");
	long    i, j, k, l, p, r;          										/* loop variables */
	long    imax, jmax;                										/* extended image boundaries */
	double  weight1, weight2, weightsum1, weightsum2, temp1, temp2;    		/* time saving variables */
	double  **f;                       							    		/* work copy of image u1 */
	double  **g;                       										/* work copy of image u2 */
	long    rmin, rmax, smin, smax;    										/* boundaries of search window */
	double weight3, weight4, weightsum3, weightsum4, tempself1, tempself2;  /* time saving variables self similarity */
	double gix,gpx,giy,gpy,fix,fpx,fiy,fpy;									/* gradient values */
	/* create reflecting dummy boundaries */
	dummies (u1, nx, ny, m);
	dummies (u2, nx, ny, m);

	imax = nx + m - 1;
	jmax = ny + m - 1;

	alloc_matrix (&f, nx+2*m+2, ny+2*m+2);
	alloc_matrix (&g, nx+2*m+2, ny+2*m+2);
	/* copy u into f  */
	for (i=0; i<nx+2*m; i++)
	{
		for (j=0; j<ny+2*m; j++)
		{
			f[i][j]=u1[i][j];
			g[i][j]=u2[i][j];
		}
	}
	dummies (f, nx+2*m, ny+2*m, 1);
	dummies (g, nx+2*m, ny+2*m, 1);


	/* ---- NL-Means Filter ---- */

	/* iterate over all pixels u[i][j] */
	for (i=m; i<=imax; i++)
	{
		for (j=m; j<=jmax; j++)
		{
			u1[i][j]   = 0.0;
			u2[i][j]   = 0.0;
			x[i][j]    = 0.0;
			y[i][j]    = 0.0;
			weightsum1 = 0.0;
			weightsum2 = 0.0;
			weightsum3 = 0.0;
			weightsum4 = 0.0;
			/* compute boundaries of the search window */
			if (i-t < m)     rmin = m;
			else             rmin = i-t;
			if (i+t > imax)  rmax = imax;
			else             rmax = i+t;
			if (j-t < m)     smin = m;
			else             smin = j-t;
			if (j+t > jmax)  smax = jmax;
			else             smax = j+t;

			/* for each pixel in the search window... */
			for (k=rmin; k<=rmax; k++)
			{
				for (l=smin; l<=smax; l++)
				{
					temp1=0.0;
					temp2=0.0;
					tempself1=0.0;
					tempself2=0.0;
					/* compute patch distance (squared Euclidean norm using gradient) */
					/* goal 1=1 computes single NL-means measure */
					if(goal1==1)
					{
						for (p=-m; p<=m; p++)
						{
							for (r=-m; r<=m; r++)
							{
								/* For gradient forward difference is used */
								gpx   = (g[k-p+1][l-r]-g[k-p][l-r]);
								fix   = (f[i-p+1][j-r]-f[i-p][j-r]);
								gpy   = (g[k-p][l-r+1]-g[k-p][l-r]);
								fiy   = (f[i-p][j-r+1]-f[i-p][j-r]);
								temp2+= ((fix-gpx)*(fix-gpx))+((fiy-gpy)*(fiy-gpy));
							}
						}
					}
					/* goal 1=2 computes two NL-means measure */
					if(goal1==2)
					{
						for (p=-m; p<=m; p++)
						{
							for (r=-m; r<=m; r++)
							{
								gpx       = (g[k-p+1][l-r]-g[k-p][l-r]);
								fix       = (f[i-p+1][j-r]-f[i-p][j-r]);
								fpx       = (f[k-p+1][l-r]-f[k-p][l-r]);
								gpy       = (g[k-p][l-r+1]-g[k-p][l-r]);
								fiy       = (f[i-p][j-r+1]-f[i-p][j-r]);
								fpy       = (f[k-p][l-r+1]-f[k-p][l-r]);
								temp2    += ((fix-gpx)*(fix-gpx))+((fiy-gpy)*(fiy-gpy));
								tempself1+= ((fix-fpx)*(fix-fpx))+((fiy-fpy)*(fiy-fpy));
							}
						}
					}
					/* goal 1=3 computes four NL-means measure */
					if(goal1==3)
					{
						for (p=-m; p<=m; p++)
						{
							for (r=-m; r<=m; r++)
							{
								gix       = (g[i-p+1][j-r]-g[i-p][j-r]);
								gpx       = (g[k-p+1][l-r]-g[k-p][l-r]);
								fix       = (f[i-p+1][j-r]-f[i-p][j-r]);
								fpx       = (f[k-p+1][l-r]-f[k-p][l-r]);
								giy       = (g[i-p][j-r+1]-g[i-p][j-r]);
								gpy       = (g[k-p][l-r+1]-g[k-p][l-r]);
								fiy       = (f[i-p][j-r+1]-f[i-p][j-r]);
								fpy       = (f[k-p][l-r+1]-f[k-p][l-r]);
								temp1    += ((gix-fpx)*(gix-fpx))+((giy-fpy)*(giy-fpy));
								temp2    += ((fix-gpx)*(fix-gpx))+((fiy-gpy)*(fiy-gpy));
								tempself1+= ((fix-fpx)*(fix-fpx))+((fiy-fpy)*(fiy-fpy));
								tempself2+= ((gix-gpx)*(gix-gpx))+((giy-gpy)*(giy-gpy));
							}
						}
					}


					/* compute the corresponding Gaussian weight. note that temp1 and temp2
                   already contains the squared Euclidean norm */
					if(goal1==1)
					{
						weight2     = exp (-temp2 / (2 * delta * delta));
						u2[i][j]   += weight2 * g[k][l];
						weightsum2 += weight2;
					}
					if(goal1==2)
					{
						weight2     = exp (-temp2 / (2 * delta * delta));
						weight3     = exp (-tempself1 / (2 * delta * delta));
						u2[i][j]   += weight2 * g[k][l];
						x[i][j]    += weight3 * f[k][l];
						weightsum2 += weight2;
						weightsum3 += weight3;
					}
					if(goal1==3)
					{
						weight1     = exp (-temp1 / (2 * delta * delta));
						weight2     = exp (-temp2 / (2 * delta * delta));
						weight3     = exp (-tempself1 / (2 * delta * delta));
						weight4     = exp (-tempself2 / (2 * delta * delta));
						u1[i][j]   += weight1 * f[k][l];
						u2[i][j]   += weight2 * g[k][l];
						x[i][j]    += weight3 * f[k][l];
						y[i][j]    += weight4 * g[k][l];
						weightsum1 += weight1;
						weightsum2 += weight2;
						weightsum3 += weight3;
						weightsum4 += weight4;
					}
				}

			}

			/* normalize result */
			if (weightsum1 > 0)
			{

				u1[i][j] = u1[i][j]/weightsum1;
			}

			else
			{
				u1[i][j] = f[i][j];
			}
			if (weightsum2 > 0)
			{
				u2[i][j] = u2[i][j]/weightsum2;
			}
			else
			{

				u2[i][j] = g[i][j];
			}

			if (weightsum3 > 0)
			{

				x[i][j] = x[i][j]/weightsum3;
			}
			else
			{

				x[i][j] = u2[i][j];
			}

			if (weightsum4 > 0)
			{

				y[i][j] = y[i][j]/weightsum4;
			}
			else
			{

				y[i][j] = u1[i][j];
			}
		}
	}
	/* Error Value compuatation */
	/* dummies in case of gradient for boundary pixels */
	dummies (u1, nx, ny, m+1);
	dummies (u2, nx, ny, m+1);
	dummies (x, nx, ny,  m+1);
	dummies (y, nx, ny,  m+1);
	/* Derivative values */
	double fxder, fyder;
	double u2xder, u1xder, xxder, yxder;
	double u2yder, u1yder, xyder, yyder;
	double errorvalue = 0.0;				/* Overall Error Value */
	/* Illumination derivative values */
	double illumfxder = 0.0;				
	double illumfyder = 0.0;
	double illum1xder = 0.0;
	double illum1yder = 0.0;
	double illum2xder = 0.0;
	double illum2yder = 0.0;
	/* Rotational deerivative values */
	double ru2der     = 0.0;
	double rxder      = 0.0;
	double rfder      = 0.0;
	double ru1der     = 0.0;
	double ryder      = 0.0;
	for (i=m; i<=imax; i++)
	{
		for (j=m; j<=jmax; j++)
		{
			if(goal1==1)
			{
				u2xder = (u2[i+1][j]-u2[i][j]);
				fxder  = (f[i+1][j]-f[i][j]);
				u2yder = (u2[i][j+1]-u2[i][j]);
				fyder  = (f[i][j+1]-f[i][j]);
			}
			if(goal1==2)
			{
				u2xder = (u2[i+1][j]-u2[i][j]);
				xxder  = (x[i+1][j]-x[i][j]);
				u2yder = (u2[i][j+1]-u2[i][j]);
				xyder  = (x[i][j+1]-x[i][j]);
			}
			if(goal1==3)
			{
				u2xder = (u2[i+1][j]-u2[i][j]);
				u1xder = (u1[i+1][j]-u1[i][j]);
				xxder  = (x[i+1][j]-x[i][j]);
				yxder  = (y[i+1][j]-y[i][j]);
				u2yder = (u2[i][j+1]-u2[i][j]);
				u1yder = (u1[i][j+1]-u1[i][j]);
				xyder  = (x[i][j+1]-x[i][j]);
				yyder  = (y[i][j+1]-y[i][j]);
			}
			illumfxder  = (u2xder-fxder)*(u2xder-fxder);
			illumfyder  = (u2yder-fyder)*(u2yder-fyder);
			illum1xder  = (u2xder-xxder)*(u2xder-xxder);
			illum1yder  = (u2yder-xyder)*(u2yder-xyder);
			illum2xder  = (u1xder-yxder)*(u1xder-yxder);
			illum2yder  = (u1yder-yyder)*(u1yder-yyder);
			ru2der      = (sqrt((u2xder*u2xder)+(u2yder*u2yder)));
			rxder       = (sqrt((xxder*xxder)+(xyder*xyder)));
			rfder		= (sqrt((fxder*fxder)+(fyder*fyder)));
			ru1der      = (sqrt((u1xder*u1xder)+(u1yder*u1yder)));
			ryder       = (sqrt((yxder*yxder)+(yyder*yyder)));
			/* Single NL-means measure */
			if(goal1==1)
			{
				/* goal2 = 1, No invariance */
				if(goal2==1)
				{
					errorvalue += (u2[i][j]-f[i][j])*(u2[i][j]-f[i][j]);
				}
				/* goal2 = 2, Illumination invariance */
				if(goal2==2)
				{
					errorvalue += ((1-(2*alpha))*((u2[i][j]-f[i][j])*(u2[i][j]-f[i][j]))
							      +(alpha*(illumfxder+illumfyder))
					);
				}
				/* goal2 = 3, Rotational invariance */
				if(goal2==3)
				{
					errorvalue += ((1-beta)*((u2[i][j]-f[i][j])*(u2[i][j]-f[i][j]))
							      +(beta*0.5*(rfder-ru2der)*(rfder-ru2der))
					);
				}
				/* goal2 = 4, Illumination and Rotational invariance */
				if(goal2==4)
				{
					errorvalue += ((1-(2*alpha)-beta)*((u2[i][j]-f[i][j])*(u2[i][j]-f[i][j]))
							      +(alpha*(illumfxder+illumfyder))+(beta*0.5*(rfder-ru2der)*(rfder-ru2der))
					);
				}
			}
			/* Two NL-means measures */
			if(goal1==2)
			{
				/* goal2 = 1, No invariance */
				if(goal2==1)
				{
					errorvalue += (u2[i][j]-x[i][j])*(u2[i][j]-x[i][j]);
				}
				/* goal2 = 2, Illumination invariance */
				if(goal2==2)
				{
					errorvalue += ((1-(2*alpha))*((u2[i][j]-x[i][j])*(u2[i][j]-x[i][j]))
							      +(alpha*(illum1xder+illum1yder))
					);
				}
				/* goal2 = 3, Rotational invariance */
				if(goal2==3)
				{
					errorvalue += ((1-beta)*((u2[i][j]-x[i][j])*(u2[i][j]-x[i][j]))
							      +(beta*0.5*(rxder-ru2der)*(rxder-ru2der))
					);
				}
				/* goal2 = 4, Illumination and Rotational invariance */
				if(goal2==4)
				{
					errorvalue += ((1-(2*alpha)-beta)*((u2[i][j]-x[i][j])*(u2[i][j]-x[i][j]))
							      +(alpha*(illum1xder+illum1yder))+(beta*0.5*(rxder-ru2der)*(rxder-ru2der))
					);
				}
			}
			/* Four NL-means measures */
			if(goal1==3)
			{
				/* goal2 = 1, No invariance */
				if(goal2==1)
				{
					errorvalue += 0.5*((u2[i][j]-x[i][j])*(u2[i][j]-x[i][j])
							      +  
								  ((u1[i][j]-y[i][j])*(u1[i][j]-y[i][j])));
				}
				/* goal2 = 2, Illumination invariance */
				if(goal2==2)
				{
					errorvalue += 0.5*(((1-(2*alpha))*((u2[i][j]-x[i][j])*(u2[i][j]-x[i][j]))
							          +(alpha*(illum1xder+illum1yder)))
							      +
							     ((1-(2*alpha))*((u1[i][j]-y[i][j])*(u1[i][j]-y[i][j]))
									  +(alpha*(illum2xder+illum2yder))));
				}
				/* goal2 = 3, Rotational invariance */
				if(goal2==3)
				{
					errorvalue += 0.5*(((1-beta)*((u2[i][j]-x[i][j])*(u2[i][j]-x[i][j]))
									  +(beta*0.5*(rxder-ru2der)*(rxder-ru2der)))
							      +
							      ((1-beta)*((u1[i][j]-y[i][j])*(u1[i][j]-y[i][j]))
									  +(beta*0.5*(ryder-ru1der)*(ryder-ru1der))));
				}
				/* goal2 = 4, Illumination and Rotational invariance */
				if(goal2==4)
				{
					errorvalue += 0.5*(((1-(2*alpha)-beta)*((u2[i][j]-x[i][j])*(u2[i][j]-x[i][j]))
							          +(alpha*(illum1xder+illum1yder))+(beta*0.5*(rxder-ru2der)*(rxder-ru2der)))
							      +
							      ((1-(2*alpha)-beta)*((u1[i][j]-y[i][j])*(u1[i][j]-y[i][j]))
									  +(alpha*(illum2xder+illum2yder))
									  +(beta*0.5*(ryder-ru1der)*(ryder-ru1der))));
				}
			}
		}
	}
	printf("*****************************************************");
	printf ("\n");
	errorvalue=errorvalue/((imax-m+1)*(jmax-m+1));
	printf("Error value is %f\n",errorvalue);
	printf("*****************************************************");
	printf ("\n");
	disalloc_matrix (f, nx+2*m+2, ny+2*m+2);
	disalloc_matrix (g, nx+2*m+2, ny+2*m+2);
	return errorvalue;
} /* NL-means using gradient penalization */

int main ()

{
	clock_t begin, end;
	begin = clock();
	double  totaltime;			  /* Time to run the algorithm */
	char    in[80];               /* for reading data */
	char    ref[80];			  /* for reading data */
	char    out1[80];             /* for reading data */
	char    out2[80];             /* for reading data */
	char    out3[80];             /* for reading data */
	char    out4[80];             /* for reading data */
	double  **u1;                 /* Cross-Similarity of texture 1 with texture 2 as reference   */
	double  **u2;				  /* Cross-Similarity of texture 2 with texture 1 as reference   */
	double  **x;				  /* Self-Similarity of texture 1 */
	double  **y;				  /* Self-Similarity of texture 2 */
	double  **f;                  /* texture 1   */
	double  **g;                  /* texture 2  */
	double  **tmp;                /* work copy of f */
	double  **tmp2;               /* work copy of g */
	long    i, j, k, l;           /* loop variable */
	long    nx, ny;               /* image size in x, y direction */
	long    m;                    /* patch radius */
	long    t;                    /* search window size */
	double  delta1;               /* Penalization factor for NL-means */
	double  alpha;				  /* alpha value for illumination */
	double  beta;				  /* beta value for rotational invariance */
	long    imax, jmax;           /* help indices */
	double  goal,goal1,goal2;     /* goal-penalization, goal1-complexity choice for NL means, goal2-invariance choice */
	double  param;				  /* manual or automatic parameter selection */
	char    comments[1600];       /* string for comments */
	double  errorval;
	FILE *file= fopen("errorvalues.txt", "a");  /* Write error value to a text file for readability */
	if (file == NULL)
	{
	    printf("Error opening file!\n");
	    exit(1);
	}


	printf ("\n");
	printf ("NL PATCH BASED ERROR MEASURE FOR TEXTURE CLASSIFICATION:\n\n");
	printf ("**************************************************\n\n");
	printf ("    Copyright 2016 by Joachim Weickert,           \n");
	printf ("    Sven Grewenig, Pascal Peter and               \n");
	printf ("    Sreenivas Narasimha Murali.                   \n");
	printf ("    Dept. of Mathematics and Computer Science     \n");
	printf ("    Saarland University, Saarbruecken, Germany    \n\n");
	printf ("    All rights reserved. Unauthorized usage,      \n");
	printf ("    copying, hiring, and selling prohibited.      \n\n");
	printf ("    Send bug reports to                           \n");
	printf ("    weickert@mia.uni-saarland.de                  \n\n");
	printf ("**************************************************\n\n");


	/* ---- read input image (pgm format P5) ---- */
	printf ("\nFirst input image (pgm):                      ");
	read_string (in);
	read_pgm_and_allocate_memory (in, &nx, &ny, &f);
	printf("\nSecond input image should be of same resolution as first  \n");
	printf ("\nSecond input image to compare (pgm):          ");
	read_string (ref);
	read_pgm_and_allocate_memory (ref, &nx, &ny, &g);

	printf ("\n");
	//m=4;t=10;delta1=130;goal=2;goal1=3;goal2=1;alpha=0;beta=0;
	/* ---- read parameters and choices---- */
	
	printf("parameter selection:\n");
	printf("1. Automatic selection for patch size, window size, penaliation factor and time complexity choices\n");
	printf("2. Manual selection for patch size, window size, penaliation factor and time complexity choices\n");
	printf("\nMake your choice:                              ");
	read_double (&param);
	if(param == 2)
	{
		printf ("patch size (2m+1)x(2m+1) parameter m (integer):                 ");
		read_long (&m);

		printf ("search window size (2t+1)x(2t+1) parameter t (integer):         ");
		read_long (&t);
		printf ("Penalization factor (float):                ");
		read_double (&delta1);
		printf ("\nchoice 1 \n"
			"1. Euclidean distance for penalizing deviations\n"
			"2. Euclidean distance (Gradient Domain) for penalizing deviations\n"
			"Make your choice:						");
		read_double(&goal);
		if(goal!=1 && goal !=2)
		{
			goal=1;
		}
		printf ("\nchoice 2 \n"
			"\n1. Easiest,simple and fastest way to classify.\n"
			"   error measure will not be commutative and\n "
			"  single NL measure would be taken. It may \n "
			"  work decently for simple textured images\n "
			"\n2. Reference based classification: Error measure\n"
			"   will not be commutative. Two NL measures.:\n"
			"   It will work good for some complicated textures \n"
			"   but error values will be scaled high.\n "
			"\n3. Commutative Reference based classification:\n"
			"   It will work for more complicated textures and error measure\n "
			"  This will take twice the amount of time compared\n"
			"   to method 2. Four Nl measures would be made\n"
			"\n   Make your choice:					   ");
		read_double(&goal1);
		if(goal1!=1 && goal1 !=2 && goal1 !=3)
		{
			goal1=3;
		}

		printf("\n choice 3"
			"\n 1. Include no invariance"
			"\n 2. Include invariance under illumination"
			"\n 3. Include invariance under rotation"
			"\n 4. Include both invariance"
			"\n Make your choice:						");
		read_double(&goal2);
		if(goal2!=1 && goal2 !=2&& goal2 !=3 && goal2 != 4)
		{
			goal2=1;
		}
		if(goal2 == 1)
		{
			alpha=0;beta=0;
		}
		if(goal2 == 2)
		{
			printf ("\nalpha value:                            ");
			read_double (&alpha);
			beta=0;
		}
		else if(goal2 == 3)
		{
			alpha=0;
			printf ("beta value:                             ");
			read_double (&beta);
		}
		else if(goal2 == 4)
		{
			printf ("\nalpha value:                            ");
			read_double (&alpha);
			printf ("beta value:                             ");
			read_double (&beta);
		}
		else
		{
			alpha = 0;
			beta  = 0;
		}
	}
	else
	{
		m      = 4;
		t      = 10;
		delta1 = 130;
		goal   = 1;
		goal1  = 3;
		goal2  = 1;
		alpha  = 0;
		beta   = 0;
	}
//	if(goal1 == 1)
//	{
//		printf ("Output image-1 (Cross-Similarity structure: second input with first as reference)  (pgm):");
//		read_string (out2);
//		printf ("\n");
//	}
//	else if(goal1 == 2)
//	{
//		printf ("Output image-1 (Cross-Similarity structure: second input with first as reference)  (pgm):");
//		read_string (out2);
//		printf ("\n");
//		printf ("Output image-2 (Self-Similarity structure:  first input with first as reference)   (pgm):");
//		read_string (out3);
//		printf ("\n");
//	}
//	else
//	{
//		printf ("Output image-1 (Cross-Similarity structure: first input with second as reference)  (pgm):");
//		read_string (out1);
//		printf ("\n");
//		printf ("Output image-2 (Cross-Similarity structure: second input with first as reference)  (pgm):");
//		read_string (out2);
//		printf ("\n");
//		printf ("Output image-3 (Self-Similarity structure:  first input with first as reference)   (pgm):");
//		read_string (out3);
//		printf ("\n");
//		printf ("Output image-4 (Self-Similarity structure:  second input with second as reference) (pgm):");
//		read_string (out4);
//		printf ("\n");
//	}
	/* ---- allocate storage for u1 and u2 ---- */

	alloc_matrix (&u1,           nx+2*m+2, ny+2*m+2);
	alloc_matrix (&u2,           nx+2*m+2, ny+2*m+2);
	alloc_matrix (&x,            nx+2*m+2, ny+2*m+2);
	alloc_matrix (&y,            nx+2*m+2, ny+2*m+2);
	alloc_matrix (&tmp,          nx+2*m+2, ny+2*m+2);
	alloc_matrix (&tmp2,         nx+2*m+2, ny+2*m+2);


	/* ---- initialisations ---- */

	imax = nx + m - 1;
	jmax = ny + m - 1;

	for (j=m, l=1; j<=jmax; j++, l++)
	{

		for (i=m, k=1; i<=imax; i++, k++)
		{
			tmp[i][j] = u1[i][j] = f[k][l];
			tmp2[i][j]= u2[i][j] = g[k][l];
			/* Initialize Self-Similarity images as zero */
			x[i][j] = 0;
			y[i][j] = 0;
		}

	}
	/* ---- process textures with NL-means ---- */


	printf ("Applying NL-means filter for comparing textures with patch size %ld x %ld, \n", 2*m+1, 2*m+1);
	printf ("window size %ld x %ld and Penalization factor %8.4lf\n\n", 2*t+1, 2*t+1, delta1);
	/* ---- Normal euclidean (nl_means) or gradient domain euclidean (nl_means1) penalization ----*/
	if(goal == 1)
	{
		errorval = nl_means  (nx, ny, m, delta1, t, u1, u2, x, y, alpha, beta, goal1, goal2);
	}
	else if(goal == 2)
	{
		errorval = nl_means1 (nx, ny, m, delta1, t, u1, u2, x, y, alpha, beta, goal1, goal2);
	}
	else
	{
		errorval = nl_means  (nx, ny, m, delta1, t, u1, u2, x, y, alpha, beta, goal1, goal2);
	}

	/* copy result to f and g*/
	for (j=m, l=1; j<=jmax; j++, l++)
	{
		for (i=m, k=1; i<=imax; i++, k++)
		{
			f[k][l] = u1[i][j];
			g[k][l] = u2[i][j];
		}
	}

	printf("\nAlgorithm has finished running...\n");
	comments[0]='\0';

	comment_line (comments, "# NL-means error measure\n");
	comment_line (comments, "# patch size m:         %8ld\n", m);
	comment_line (comments, "# search window size n: %8.4f\n", t);
	comment_line (comments, "# Penalization factor:      %8.4f\n", delta1);

	/* write image */
//	if(goal1==1)
//	{
//		write_pgm (g, nx, ny, out2, comments);
//		printf ("Output image-1 (Cross-Similarity structure of second input with first as reference) %s successfully written\n\n", out2);
//	}
//	else if(goal1==2)
//	{
//		write_pgm (g, nx, ny, out2, comments);
//		write_pgm (x, nx, ny, out3, comments);
//		printf ("Output image-1 (Cross-Similarity structure of second input with first as reference) %s successfully written\n\n", out2);
//		printf ("Output image-2 (Self-Similarity structure of first input with first as reference)   %s successfully written\n\n", out3);
//	}
//	else
//	{
//		write_pgm (f, nx, ny, out1, comments);
//		write_pgm (g, nx, ny, out2, comments);
//		write_pgm (x, nx, ny, out3, comments);
//		write_pgm (y, nx, ny, out4, comments);
//		printf ("Output image-1 (Cross-Similarity structure of first input with second as reference) %s successfully written\n\n", out1);
//		printf ("Output image-2 (Cross-Similarity structure of second input with first as reference) %s successfully written\n\n", out2);
//		printf ("Output image-3 (Self-Similarity structure of first input with first as reference)   %s successfully written\n\n", out3);
//		printf ("Output image-4 (Self-Similarity structure of second input with second as reference) %s successfully written\n\n", out4);
//	}
//
//


	/* ---- free memory  ---- */

	disalloc_matrix (f,            nx+2,     ny+2);
	disalloc_matrix (g,            nx+2,     ny+2);
	disalloc_matrix (tmp,          nx+2*m+2, ny+2*m+2);
	disalloc_matrix (tmp2,         nx+2*m+2, ny+2*m+2);
	disalloc_matrix (u1,           nx+2*m+2, ny+2*m+2);
	disalloc_matrix (u2,           nx+2*m+2, ny+2*m+2);
	disalloc_matrix (x,            nx+2*m+2, ny+2*m+2);
	disalloc_matrix (y,            nx+2*m+2, ny+2*m+2);
	end=clock();
	totaltime=(double)(end - begin) / CLOCKS_PER_SEC;
	/* ---- Total Time taken estimation ----*/
	printf("Total time taken is %f minutes\n", totaltime/60);
	/* ---- Write error value to text file ----*/
	fprintf(file, "\nError value between %s and %s for alpha:%f and beta:%f is %f \n"
				  "(patch size: %ld, window size: %ld, penalization factor: %8.4f, complexity mode: %d,penalization mode: %d)\n"
			, in, ref, alpha, beta, errorval, m, t, delta1, (int)goal1, (int)goal);
			
	return(0);
}
